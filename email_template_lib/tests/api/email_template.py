import request
import pytest, tornado.options, subprocess, re
from email_template_lib.tests.helper.api import Api
from tornado.escape import json_encode, json_decode
pytest.email = 'niroj.adhikary@gmail.com'
pytest.password = 'password'
pytest.authorization_token = None
pytest.template_id = None
"""
test if all email template operation are working correctly
"""
"""
how to test package?
python -m pytest tests/api/email_template.py -ra -s --test_url=<url> -k 'test_can_login or test_can_get_email_templates'
-k option is to test only particular function
or expression is passed to check multiple function simultaneously
"""

class TestRole(object):
    @pytest.mark.parametrize("method, body", [
        ("POST", {
                    "email": pytest.email,
                    "password": pytest.password
                })
    ])
    def test_can_login(self, method, body):
        api = Api("login")
        response = api.call(method, body)
        pytest.authorization_token = json_decode(response.body)["data"]
        assert response.code == 200
    
    @pytest.mark.parametrize("method, body", [
        ("GET", None)
    ])
    def test_can_get_email_templates(self, method, body):
        api = Api("email-templates")
        response = api.call(method, body, pytest.authorization_token)
        assert response.code == 200


    @pytest.mark.parametrize("method, body", [
        ("POST", {
                "title": "Test Template",
                "subject": "This is test template",
                "content": "<b>Hello test template</b>",
                "type": "deal"
            })
    ])
    def test_can_create_email_templates(self, method, body):
        api = Api("email-templates")
        response = api.call(method, body, pytest.authorization_token)
        pytest.template_id = json_decode(response.body)["data"]["id"]
        assert response.code == 200

    @pytest.mark.parametrize("method, body", [
        ("PATCH", {
                "title": "Test Template Edited",
                "subject": "This is test template",
                "content": "<b>Hello test template</b>",
                "type": "deal"
            })
    ])
    def test_can_edit_email_templates(self, method, body):
        api = Api("email-templates/" + str(pytest.template_id))
        response = api.call(method, body, pytest.authorization_token)
        assert response.code == 200

    @pytest.mark.parametrize("method, body", [
        ("DELETE", None)
    ])
    def test_can_delete_email_templates(self, method, body):
        api = Api("email-templates/" + str(pytest.template_id))
        response = api.call(method, body, pytest.authorization_token)
        assert response.code == 200