from libraries.system.mapper import DataMapper
from email_template_lib.api.dtos.base import ModelDTO


class EmailTemplateDTO(DataMapper):
    def __init__(self, data):
        self.attribute = [
            'id',
            'title',
            'subject',
            'content',
            'created_by.id',
            'created_by.name',
            'type',
            'status',
            'created_at'
        ]
        super().__init__(self.attribute, data)


class EmailTemplateRequestDTO(object):
    def __init__(self):
        self.id = None
        self.title = None
        self.subject = None
        self.content = None
        self.created_by = None
        self.type = None
        self.status = None
        self.created_at = None


class EmailTemplateModelDTO(ModelDTO):
    def __init__(self, dto: EmailTemplateRequestDTO):
        self.id = dto.id
        self.title = dto.title
        self.subject = dto.subject
        self.content = dto.content
        self.created_by = dto.created_by
        self.type = dto.type
        self.status = dto.status
        self.created_at = dto.created_at
