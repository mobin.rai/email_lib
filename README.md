## Email Template package manager
<b>
Chuchuro email template package is a package with Email templates for register and forget password.
</b>

<h4>Table of contents</h4>
<hr>
<ul>
    <li><a href="#about">About</a></li>
    <li><a href="#installation-instruction">Installation Instructions</a></li>
    <li><a href="#features">Features</a></li>
    <li><a href="#compatiable">Compatibility</a></li>
    <li><a href="#requirements">Requirements</a></li>
    <li><a href="#route-list">Route List</a></li>
    <li><a href="#simple-example">Sample Example</a></li>
    <li><a href="#package-license">Profile package manager License</a></li>
    <li><a href="#links">Links</a></li>
</ul>
<h3><a id="user-content-about" class="anchor" aria-hidden="true" href="#about"></a>About</h3>   
Profile package manager is used for Email Registration Verification,User Authentication,User Authorization, Forget Password, 
User Roles and Permissions and User Profiles.

<h4><a href="#installation-instruction"></a>Installation Instructions</h4>
 1. In the terminal, run command from below to install package.
    <pre>pip3 install chuchuro-email-template</pre>
 2. Run compilation script. (<pre>email-template-compile</pre>).<br>
 3. In the terminal, run command from below to migrate files from package. <br>
    <pre>email-template-run-migration --env=dev_(developer_name)</pre>

    <h4>To upgrade package</h4>
    <pre>pip3 install chuchuro-email-template --upgrade</pre></pre>    
<h4><a href="#features"></a>Features</h4>
<table>
<tr><td>Features</td></tr>
<tr><td>Built on Python 3.6</td></tr>
<tr><td>Uses Postgres (can be changed)</td></tr>
<tr><td>CRUD (Create, Read, Update, Delete) Email Template</td></tr>
<tr><td>Adds default email template for register and forget password.</td></tr>
</table>

<h4><a href="#compatiable"></a>Compatibility</h4>
Package is compatible with:<br>
    <pre>python >=3.6</pre>

<h4><a href="#requirements"></a>Requirements</h4>
<blockquote>
<a href="https://maciejkula.github.io/spotlight/" target="_blank">Spotlight</a><br>
<pre>
Laravel style data validation for Python.
</pre>
</blockquote>

<blockquote>
<a href="https://click.palletsprojects.com/en/7.x/" target="_blank">Click</a><br>
<pre>
Click is a Python package for creating beautiful command line interfacesin a composable way with 
as little code as necessary.
</pre>
</blockquote>
<blockquote>
<a href="https://github.com/coleifer/peewee" target="_blank">Peewee</a><br>
<pre>
Peewee is a simple and small ORM. It has few (but expressive) concepts,making it easy 
to learn and intuitive to use.
</pre>
</blockquote>
<h4><a href="#route-list"></a>Route List</h4>
<pre>
Email Template - <span class="pl-e">'/api/email-templates[/]?([\d]+)?'</span>
</pre>
<h4><a href="#simple-example"></a>Sample Example</h4>
Importing Login class from to /config/services.php

<pre>
<span class="pl-k">from</span> email_template.controllers.email_template <span class="pl-k">import</span> EmailTemplate

<span class="pl-k">class</span><span class="pl-e"> UserLogin</span>(<span class="pl-e">Login,AbstractClass</span>):

   <span class="pl-c1">pass</span>

</pre>

<h4><a href="#package-license"></a>Profile package manager License</h4>
Profile package manager License is licensed under the <a href="" target="_blank">MIT</a> license. Enjoy!

<h4><a href="#links"></a>Links</h4>
Releases: https://pypi.org/project/profile-package-manager/


