import glob, os, datetime
from imp import load_source
from application.settings import application_settings
from libraries.system.migration.model import MigrationModel
from libraries.system.migration.manager import MigrationManager

"""
    Runs all the migration files from migrations directory 
    and 
    adds email templates from email_temapltes template_files directory
"""
class EmailTemplateMigrationManager(MigrationManager):
    def __init__(self):
        self.current_file = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

    def _get_defined_migrations(self):
        """ Returns list of all the migrations that are defined in the application """
        migration_dir = self.current_file+'/data/migrations'

        defined_migration_list = glob.glob(migration_dir + '/[!_]*.*')
        # print(defined_migration_list)
        defined_migration_dict = {}
        for defined_migration in defined_migration_list:
            migration_name = os.path.basename(defined_migration)
            migration_name = migration_name.replace(".py", "")
            defined_migration_dict[migration_name] = defined_migration

        return defined_migration_dict

    def run(self):
        MigrationModel.create_table(True)
        self._set_pending_migrations()
        self._migrate()

    def _migrate(self):
        total_pending_migrations = len(self._pending_migrations.keys())
        if total_pending_migrations > 0:
            print("%s new migration(s) collected." % total_pending_migrations)
            for migration_name, migration in self._pending_migrations.items():
                try:
                    migration_module = load_source('Migration', migration)
                    migration_module = migration_module.Migration()
                    getattr(migration_module, self.get_migration_action())()
                    print(
                        "migration script %s:%s executed successfully." % (migration_name, self.get_migration_action()))
                except Exception as e:
                    print(e)
                else:
                    if self.get_migration_action() == 'up':
                        MigrationModel.create(name=migration_name, action=self.get_migration_action())
                    if self.get_migration_action() == 'down':
                        MigrationModel.delete().where(MigrationModel.name == migration_name,
                                                      MigrationModel.action == 'up').execute()
        else:
            print("No new migration collected.")