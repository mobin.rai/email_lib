import sys, click, os, datetime
parent_dir = os.path.abspath(os.getcwd())
sys.path.append(parent_dir)
from libraries.system.application import Application
from distutils.core import setup
from Cython.Build import cythonize
import subprocess
import pathlib

@click.group(chain=True)
def cli():
    pass


class DatabaseMigration(click.MultiCommand):

    def run_migration(self, action='up'):
        from email_template_lib.services.migration import EmailTemplateMigrationManager
        email_template_migration = EmailTemplateMigrationManager()
        email_template_migration.set_migration_action(action)
        email_template_migration.run()


@cli.command('email-template-run-migration')
@click.option('--env', help='Application environment')
@click.option('--action', help='Methods defined in migration', default='up')
def migrate(env, action):
    application = Application()
    application.set_application_environment(env)
    application.init_settings()

    db_migration_tool = DatabaseMigration()
    db_migration_tool.run_migration(action)


@cli.command('compile')
def compile():
    current_directory = str(pathlib.Path(__file__).parent.absolute().parent.absolute())
    modules = [
        current_directory + '/api/email_template.py',
        current_directory + '/api/dtos/base.py',
        current_directory + '/api/dtos/email_template.py',
        current_directory + '/data/models/email_template.py',
        current_directory + '/data/repository/email_template_data.py',
        current_directory + '/services/email_template.py',
        current_directory + '/services/migration.py'
    ]
    for module in modules:
        try:
            subprocess.call("cythonize -f -i " + module, shell=True)
        except Exception as e:
            print(e)
        finally:
            os.remove(module)

if __name__ == '__main__':
    cli()