from libraries.system.model import BaseModel
from peewee import datetime, IntegerField, CharField, TextField, ForeignKeyField
from playhouse.postgres_ext import DateTimeTZField
from profilelib.data.models.user import UserModel


class EmailTemplateModel(BaseModel):
    class Meta:
        table_name = "email_templates"
    title = CharField()
    subject = TextField()
    content = TextField()
    created_by = ForeignKeyField(UserModel, index=True, null=True)
    type = CharField(choices=[
                                ('sign_up', 'SignUp'),
                                ('forgot_password', 'Forgot Password'),
                                ('deal', 'Deal')
                            ], null=True)
    status = CharField(choices=[
                                    ('active', 'Active'),
                                    ('inactive', 'Inactive')
                                ], default='active')
    created_at = DateTimeTZField(null=True)
    updated_at = DateTimeTZField(default=datetime.datetime.now)
    deleted_at = DateTimeTZField(null=True)