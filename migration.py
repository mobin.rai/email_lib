import sys, time, click
from __init__ import migration_path
from shutil import copyfile


class DatabaseMigration(click.MultiCommand):
    """
        @var List: Migration template path
        @example: python -m migration create --name=<migration_name>
    """
    _migration_template_path = "_template.py"
 
    def create_migration(self, migration_name):
        """ Creates migration file """
        try:
            arg_index = sys.argv.index('create')
            current_time_stamp = time.strftime("%Y%m%d%H%M%S", time.localtime())
            migration_file = "%s/%s_%s.py" % (
                migration_path,
                current_time_stamp,
                migration_name
            )
            copyfile(self._migration_template_path, migration_file)
        except IndexError:
            print("Please input the migration name")


@click.group(chain=True)
def cli():
    pass

    
@cli.command('create')
@click.option('--name', help='Migration name')
def create(name):
    dbMigrationTool = DatabaseMigration()
    dbMigrationTool.create_migration(name)


if __name__ == '__main__':
    cli()
        