import uuid
import peewee
from dependency_injector import providers, containers
from email_template_lib.services.email_template import EmailTemplateService
from email_template_lib.data.repository.email_template_data import EmailTemplateData
from email_template_lib.data.models.email_template import EmailTemplateModel


class ServiceRegistrar(containers.DeclarativeContainer):
    email_template_service = providers.Factory(EmailTemplateService,
                                               repository=EmailTemplateData(EmailTemplateModel)
                                               )
