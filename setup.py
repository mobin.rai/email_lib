from setuptools import setup,find_packages
from setuptools.command.test import test


with open('README.md', 'r') as f:
    long_description = f.read()

setup(
    name='chuchuro-email-template-lib',
<<<<<<< HEAD
    version='0.0.31',
=======
    version='0.0.30',
>>>>>>> 9ee15b1ca7a764c54112f0c8c0bca3fa4170939b
    author="Chuchuro Firm",
    author_email="",
    description="Chuchuro Email Template package manager",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/chuchuro-python-packages/emailtemplatelib",
    packages=find_packages(),
    package_dir={'email_template_lib': 'email_template_lib'},
    python_requires='>=3.6',
    setup_requires=['pytest-runner'],
    tests_require=['pytest'],
    test_suite="package_email_template_lib_manager.tests",
    entry_points='''
        [console_scripts]
        email-template-run-migration = email_template_lib.utils.cli:migrate
        email-template-compile = email_template_lib.utils.cli:compile
    ''',
    install_requires=[
        'Click',
        'spotlight',
        'dependency-injector',
        'cython'
    ],
    package_data={'email_template_lib': [
                                            '*/*.pyx',
                                            '*/*/*.pyx',
                                            '*/*/*.txt',
                                            'tests/*',
                                            'tests/**/*'
                                        ],
                  'email_template_lib.libraries.email_templates.template_files': ['*.txt']},
    include_package_data=True,
    classifiers=[
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
        "License :: OSI Approved :: MIT License"
    ],
)