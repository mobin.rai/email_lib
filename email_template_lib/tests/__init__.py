import re
import tornado.options

package_settings = {
    'api_url': None
}

options = tornado.options.parse_command_line()
for option in options:
    if re.search("--test_url", option):
        package_settings['api_url'] = option.replace('--test_url=','')
        break

if package_settings['api_url'] is None or len(package_settings['api_url']) == 0:
    raise Exception('--test_url is requires')