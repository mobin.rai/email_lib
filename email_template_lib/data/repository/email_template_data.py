import peewee
from libraries.system.model import BaseModel
from tornado.escape import json_decode, json_encode
from libraries.system.repository.base import Data
from email_template_lib.api.dtos.email_template import EmailTemplateDTO, EmailTemplateModelDTO
from peewee import SQL, fn
from libraries.system.logger import Log


class EmailTemplateData(Data):
    def __init__(self, model):
        self.model = model
        super().__init__(model)

    def search(self, dto: EmailTemplateModelDTO):
        try:
            if isinstance(dto.status, str):
                dto.status = json_decode(dto.status)
            if isinstance(dto.type, str):
                dto.type = json_decode(dto.type)
            query: peewee.ModelSelect = self.model.select(self.model)

            if dto.title is not None:
                query = query.where(self.model.title.contains(dto.title))
            if len(dto.status) and dto.status is not None and isinstance(dto.status, list):
                query = query.where(self.model.status.in_(dto.status))

            if len(dto.type) and dto.type is not None and isinstance(dto.type, list):
                query = query.where(self.model.type.in_(dto.type))

            if dto.start_date is not None and dto.end_date is not None:
                query = query.where(SQL(
                    '"email_template_model"."created_at"::date BETWEEN %s AND %s',
                    [dto.start_date, dto.end_date])
                )
            return query
        except Exception as e:
            Log().entry(e)


