import pytest, tornado.options, subprocess, re
from email_template_lib.tests.helper.api import Api
"""
test if all migration can run successfully
how to test package?
python -m pytest tests/api/migration.py -ra -s --env=dev_{developer} -k 'test_can_migrate'
"""

class TestMigration(object):

    def test_can_migrate(self):
        options = tornado.options.parse_command_line()
        for option in options:
            if re.search("--env", option):
                env = option.replace('--env=', '')
                break
        subprocess.call("email-template-run-migration --env=" + env, shell=True)