from email_template_lib.api.dtos.email_template import EmailTemplateModelDTO, EmailTemplateRequestDTO
from spotlight.validator import Validator


class EmailTemplateValidationRules:
    def __init__(self):
        self.validator = Validator()

    def validate_create_email_template(self, dto: EmailTemplateRequestDTO) -> list:
        email_template_dto = EmailTemplateModelDTO(dto)
        data = {
            "title": email_template_dto.title,
            "subject": email_template_dto.subject,
            "content": email_template_dto.content,
            "type": email_template_dto.type
        }
        rules = {
                "title": "required|string|max:255",
                "subject": "required|string|max:300",
                "content": "required|string",
                "type": "required|string"
        }
        return self.validator.validate(data, rules)