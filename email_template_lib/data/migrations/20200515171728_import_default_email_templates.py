from libraries.system.model import BaseModel
from email_template_lib.api.dtos.email_template import EmailTemplateDTO, EmailTemplateRequestDTO
from email_template_lib.services import ServiceRegistrar

class Migration(BaseModel):

    def up(self):
        self._service = ServiceRegistrar.email_template_service()
        email_templates_directory = self.current_file + '/data/template_files'
        try:
            for files in os.listdir(email_templates_directory):
                if files == '__init__.py':
                    continue
                is_file = os.path.isfile(email_templates_directory + '/' + files)
                if is_file:
                    with open(email_templates_directory + '/' + files) as f:
                        content = f.read()
                        name = os.path.splitext(os.path.basename(files))[0]
                        title = name.replace("_", " ").capitalize()
                        email_template_request_dto = EmailTemplateRequestDTO()
                        email_template_request_dto.title = title
                        email_template_request_dto.subject = title
                        email_template_request_dto.content = content
                        email_template_request_dto.type = name
                        email_template_request_dto.status = "active"
                        email_template_request_dto.created_at = datetime.datetime.now()
                        result: EmailTemplateDTO = self._service.save_email_template(email_template_request_dto)
        except Exception as e:
            print(e)

    def down(self):
        pass
