from email_template_lib.tests import package_settings
from tornado.escape import json_encode, json_decode
from tornado.httpclient import HTTPRequest, HTTPClient
import urllib

class Api():

	def __init__(self, endpoint=None):
		self.api_url = package_settings['api_url']
		if endpoint is not None:
			self.api_url += endpoint
			
	def call(self, method=None, body=None, token=None, image=False):
		""" 
            synchronous request
        """
		request_payload = {
			'validate_cert':False,
			'method':method,
			'body':None if body is None else urllib.parse.urlencode(body)
		}
		if image is False:
			request_payload['headers'] = {
				'content-type': 'application/x-www-form-urlencoded'
			}
		else:
			request_payload['headers'] = {
				'content-type': 'multipart/form-data'
			}
		if token is not None:
			request_payload['headers']['Authorization'] = 'Bearer ' + token
		http_request_obj = HTTPRequest(self.api_url, **request_payload)        
		http_client = HTTPClient()
		response = http_client.fetch(http_request_obj)
		return response