import uuid
from email_template_lib.api.dtos.email_template import EmailTemplateDTO, EmailTemplateRequestDTO, EmailTemplateModelDTO
from libraries.system.logger import Log

class EmailTemplateService(object):

    def __init__(self, repository):
        self.repository = repository

    def get_email_template(self, dto: EmailTemplateRequestDTO) -> EmailTemplateDTO:
        email_template_model_dto = EmailTemplateModelDTO(dto)
        if email_template_model_dto.id is not None:
            result: EmailTemplateModelDTO = self.repository.get(
                                            email_template_model_dto
                                        )
        else:
            result: EmailTemplateModelDTO = self.repository.get_all(
                                                email_template_model_dto
                                            )
        return EmailTemplateDTO(result).__dict__()

    def save_email_template(self, dto: EmailTemplateRequestDTO) -> EmailTemplateDTO:
        email_template_model_dto = EmailTemplateModelDTO(dto)
        result: EmailTemplateModelDTO = self.repository.create(
                                            email_template_model_dto
                                        )
        return EmailTemplateDTO(result).__dict__()

    def create_email_template(self, dto: EmailTemplateRequestDTO) -> dict:
        response: dict = {
                            'status': False,
                            'message': 'Failed to add email.',
                            'data': None
                        }
        try:
            email_template_dto = EmailTemplateModelDTO(dto)
            result: EmailTemplateModelDTO = self.repository.create(email_template_dto)
            if result.id:
                response['status'] = True
                response['message'] = 'Email added successfully.'
                response['data'] = EmailTemplateDTO(result).__dict__()
        except Exception as e:
            Log().entry(e)
        finally:
            return response

    def update_email_template(self, dto: EmailTemplateRequestDTO, where_dto: EmailTemplateRequestDTO) -> dict:
        response: dict = {
                            'status': False,
                            'message': 'Failed to update email.',
                            'data': None
                        }
        try:
            email_template_update_dto = EmailTemplateModelDTO(dto)
            email_template_where_dto = EmailTemplateModelDTO(where_dto)
            result: int = self.repository.update(email_template_update_dto, email_template_where_dto)
            if result:
                response['status'] = True
                response['message'] = 'Email template updated successfully.'
        except Exception as e:
            Log().entry(e)
        finally:
            return response

    def delete_email_template(self, dto: EmailTemplateRequestDTO) -> dict:
        response: dict = {
                            'status': False,
                            'message': 'Failed to delete email template.',
                            'data': None
                        }
        try:
            email_template_dto = EmailTemplateModelDTO(dto)
            result: int = self.repository.delete(email_template_dto)
            if result:
                response['status'] = True
                response['message'] = 'Email template deleted successfully.'
        except Exception as e:
            Log().entry(e)
        finally:
            return response

    def search_email_template(self, dto: EmailTemplateRequestDTO) -> EmailTemplateDTO:
        try:
            result: EmailTemplateModelDTO = self.repository.search(dto)
            return EmailTemplateDTO(result).__dict__()
        except Exception as e:
            Log().entry(e)
