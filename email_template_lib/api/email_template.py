import datetime
from email_template_lib.services import ServiceRegistrar
from email_template_lib.api.dtos.email_template import EmailTemplateDTO, EmailTemplateRequestDTO
from email_template_lib.api.validations.validation import EmailTemplateValidationRules
from profilelib.services.acl import Acl
from libraries.system.logger import Log


class EmailTemplate():
    resource_slug = 'email_template'

    def __init__(self, application, request, **kwargs):
        self._service = ServiceRegistrar
        super().__init__(application, request, **kwargs)

    @Acl.permission('read', resource_slug)
    def get(self, id: int = None) -> dict:
        try:
            email_template_dto = EmailTemplateRequestDTO()
            email_template_dto.id = id
            email_template: EmailTemplateDTO = self._service.email_template_service().get_email_template(email_template_dto)
            self.set_data(email_template)
        except Exception as e:
            Log().entry(e)
            self.set_status_code(403)
            self.set_response_status(self.RESPONSE_STATUS_SUCCESS_FALSE)
        finally:
            self.send()

    @Acl.permission('write', resource_slug)
    def post(self, param=None) -> dict:
        try:
            email_template_dto = EmailTemplateRequestDTO()
            email_template_dto.title = self.get_argument('title', None)
            email_template_dto.subject = self.get_argument('subject', None)
            email_template_dto.content = self.get_argument('content', None)
            email_template_dto.type = self.get_argument('type', None)
            email_template_dto.user_id = self.get_user('id')

            validations = EmailTemplateValidationRules()
            errors = validations.validate_create_email_template(email_template_dto)
            if len(errors):
                raise ValueError(errors)
            email_template_dto.created_at = datetime.datetime.now()
            result: EmailTemplateDTO = self._service.email_template_service().create_email_template(email_template_dto)
            self.set_data(result['data'])
            self.set_response_status(result['status'])
            self.set_message(result['message'])
        except ValueError as e:
            self.set_message(str(e))
            self.set_status_code(400)
            self.set_response_status(self.RESPONSE_STATUS_SUCCESS_FALSE)
        except Exception as e:
            Log().entry(e)
            self.set_status_code(403)
            self.set_response_status(self.RESPONSE_STATUS_SUCCESS_FALSE)
        finally:
            self.send()

    @Acl.permission('edit', resource_slug)
    def patch(self, id: int = None) -> dict:
        try:
            request_dto = EmailTemplateRequestDTO()
            request_dto.title = self.get_argument('title', None)
            request_dto.subject = self.get_argument('subject', None)
            request_dto.content = self.get_argument('content', None)
            request_dto.type = self.get_argument('type', None)
            request_dto.updated_at = datetime.datetime.now()
            validations = EmailTemplateValidationRules()
            errors = validations.validate_create_email_template(request_dto)
            if len(errors):
                raise ValueError(errors)
            where_dto = EmailTemplateRequestDTO()
            where_dto.id = id
            result: EmailTemplateDTO = self._service.email_template_service().update_email_template(request_dto, where_dto)
            self.set_response_status(result['status'])
            self.set_message(result['message'])
        except ValueError as e:
            self.set_message(str(e))
            self.set_status_code(400)
            self.set_response_status(self.RESPONSE_STATUS_SUCCESS_FALSE)

        except Exception as e:
            Log().entry(e)
            self.set_response_status(self.RESPONSE_STATUS_SUCCESS_FALSE)
            self.set_status_code(403)
        finally:
            self.send()

    @Acl.permission('delete', resource_slug)
    def delete(self, id: int = None) -> str:
        try:
            request_dto = EmailTemplateRequestDTO()
            request_dto.id = id
            result: EmailTemplateDTO = self._service.email_template_service().delete_email_template(request_dto)
            self.set_response_status(result['status'])
            self.set_message(result['message'])
        except Exception as e:
            Log().entry(e)
            self.set_response_status(self.RESPONSE_STATUS_SUCCESS_FALSE)
            self.set_status_code(403)
        finally:
            self.send()


class SearchEmailTemplate:
    resource_slug = 'email_template'
    
    def __init__(self, application, request, **kwargs):
        self._service = ServiceRegistrar
        super().__init__(application, request, **kwargs)

    @Acl.permission('read', resource_slug)
    def post(self, param=None):
        # email template search
        try:
            request_dto = EmailTemplateRequestDTO()
            request_dto.title = self.get_argument('query', None)
            request_dto.status = self.get_argument('status', None)
            request_dto.type = self.get_argument('type', None)
            request_dto.start_date = self.get_argument('start_date', None)
            request_dto.end_date = self.get_argument('end_date', None)

            result: EmailTemplateDTO = self._service.email_template_service().search_email_template(request_dto)
            self.set_data(result)
        except Exception as e:
            Log().entry(e) # log error
            self.set_status_code(403)
            self.set_response_status(self.RESPONSE_STATUS_SUCCESS_FALSE)
        finally:
            self.send()
